module Main where
import Data.Char as Char
import System.IO
--main = putStrLn "Haskell Exercises"

signum x =
    if x < 0 then
        -1
    else if x > 0then
        1
    else
        0
weird x =
    case x of{
        0 -> 1;
        1 -> 5;
        2 -> 2;
        _ -> -1;
    }

weirds 0 = 1

weirds 1 = 5

weirds 2 = 2

weirds _ = -1

roots a b c =
    let determinant = sqrt(b * b - 4 * a * c)
        twicea = 2 * a
    in ((-b + determinant)/twicea, (-b - determinant)/twicea)


{-| EXERCISE 3.1
We’ve seen that multiplication binds more tightly than division.
Can you think of a way to determine whether function application binds more or less tightly than multiplication?
-}
operate p q = sqrt p * q
{-
    operate on p q results in (sqrt p) * q instead of sqrt(p * q)
-}

{- EXERCISE 3.2
Use a combination of fst and snd to extract the character out of the tuple ((1,’a’),"foo").
-}
fnchar tuple =
    snd(fst tuple)

{-| EXERCISE 3.3
Use map to convert a string into a list of booleans, each element in the new list representing whether or not the original element was a lower-case character.
That is, it should take the string “aBCde” and return [True,False,False,True,True].
-}
areLower [] = []
areLower (a:bc) =
    Char.isLower a : areLower bc

{-| EXERCISE 3.4
Use the functions mentioned in this section (you will need two of them) to compute the number of lower-case letters in a string. For instance, on “aBCde” it should return 3.
-}
lowerCount [] = 0
lowerCount (a:[]) =
    if Char.isLower a then
        1 + lowerCount []
    else 0 + lowerCount []
lowerCount (a:xs) =
    lowerCount (a:[]) + lowerCount xs

{-| EXERCISE 3.5
We’ve seen how to calculate sums and products using folding functions.
Given that the function max returns the maximum of two numbers, write a function using a fold that will return the maximum value in a list (and zero if the list is empty).
So, when applied to [5,10,2,8,1] it will return 10. Assume that the values in the list are always ≥ 0. Explain to yourself why it works.
-}
maxOfList [] = 0
maxOfList (a:cd)=
    foldr max 0 (a:cd)

{-| EXERCISE 3.6
Write a function that takes a list of pairs of length at least 2 and returns the first component of the second element in the list. So, when provided with [(5,’b’),(1,’c’),(6,’a’)], it will return 1.
-}
fstOfSnd ((a,b):(c,d):_) =
    c


{-| EXERCISE 3.7
The fibonacci sequence is defined by:
F(n) = {    1                   n = 1 or n = 2
            F(n−2) + F(n−1)     otherwise

Write a recursive function 'fib' that takes a positive integer n as a parameter and calculates Fn.
-}
fib 1 = 1
fib 2 = 1
fib number =
    fib(number - 2) + fib(number - 1)

{-| EXERCISE 3.8
Define a recursive function mult that takes two positive integers a and b and returns a*b, but only uses addition (i.e., no fair just using multiplication). Begin by making a mathematical definition in the style of the previous exercise and the rest of this section.

F(a, b)  = {    0               a = 0
                0               b = 0
                b               a = 1
                F(a-1, b)       otherwise

-}

multi a 0 = 0
multi 0 b = 0
multi 1 b = b
multi a b =
    multi (a - 1) b + b

{-| EXERCISE 3.9
Define a recursive function my map that behaves identically to the standard function map.
-}
jcmap funq [] = []
jcmap funq (a:b) =
    funq a :jcmap funq b

{-| EXERCISE 3.10
Write a program that will repeatedly ask the user for numbers until she
types in zero, at which point it will tell her the sum of all the numbers, the product of all the numbers, and, for each number, its factorial. For instance, a session might look like:
-}
--module Main where
--import System.IO
--main = do
--    hSetBuffering stdin LineBuffering
--    doAskForNumber

doAskForNumber = do
    numbers <- readNumbers
    putStrLn ("Sum of Numbers: " ++ show(sum numbers))
    putStrLn ("Product of Numbers: " ++ show(product numbers))
    printFactorials numbers

readNumbers = do
    putStrLn "Give me a numer or zero to stop"
    num <- getLine
    if (read num) == 0 then
        return []
        else do
            remaining <- readNumbers
            return ((read num ):remaining)

fact 1 = 1
fact n = n * fact (n - 1)

printFactorials [] = return ()
printFactorials (a:bc) = do
    putStrLn (show a ++ " factorial is " ++ show (fact a))
    printFactorials bc


{-| EXERCISE 4.3
Figure out for yourself, and then verify the types of the following expressions,
if they have a type. Also note if the expression is a type error:
1. \x -> [x]
2. \x y z -> (x,y:z:[])
3. \x -> x + 5
4. \x -> "hello, world"
5. \x -> x ’a’
6. \x -> x x
7. \x -> x + x
-}
{-|
1. (\(x) -> [x]) :: forall t. t -> [t]
2. Type Error
3. (\(x) -> x + 5) :: forall a. Num a => a -> a
4. (\(x) -> "hello, world") :: forall t. t -> [Char]
5. (\(x) -> x 'a') :: forall t. (Char -> t) -> t
6. Type Error
7. (\(x) -> x + x) :: forall a. Num a => a -> a
-}

{-| EXERCISE 4.4
Write a data type declaration for Triple, a type which contains three elements, all of different types. Write functions tripleFst, tripleSnd and tripleThr to extract respectively the first, second and third elements
-}
data Triple a b c = Triple a b c
tripleFst (Triple a b c) = a
tripleSnd (Triple a b c) = b
tripleThr (Triple a b c) = c

{-| EXERCISE 4.5
Write a datatype Quadruple which holds four elements. However, the first two elements must be the same type and the last two elements must be the same type. Write a function firstTwo which returns a list containing the first two elements
and a function lastTwo which returns a list containing the last two elements. Write type signatures for these functions
-}

data Quadruple a b = Quadruple a a b b
firstTwo (Quadruple a b c d) = a:b:[]
lastTwo (Quadruple a b c d) = c:d:[]
--not working yet